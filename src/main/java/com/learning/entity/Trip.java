package com.learning.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Trip {
    private int id;
    private String name;
    private String country;

    public Trip(int id, String name, String country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }

    public Trip(){}


}
