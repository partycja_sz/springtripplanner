package com.learning.dao;

import com.learning.entity.Trip;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class TripDao {
    private static Map<Integer, Trip> trips;

    static {
        trips = new HashMap<Integer, Trip>(){
            {
                put(1,new Trip(1, "Asia", "China"));
                put(2,new Trip(2, "Europe", "Estonia"));
                put(3,new Trip(3, "Europe", "Spain"));
                put(4,new Trip(4, "Africa", "Morocco"));

            }
        };
    }

    public Collection<Trip> getAllTrips(){
        return this.trips.values();
    }

    public Trip getTripById(int id){
        return this.trips.get(id);
    }

    public void deleteTripById(int id) {
        this.trips.remove(id);
    }

    public void updateTrip(Trip trip) {
        Trip updatedTrip = trips.get(trip.getId());
        updatedTrip.setCountry(trip.getCountry());
        updatedTrip.setName(trip.getName());
        trips.put(trip.getId(), trip);
    }

    public void addTrip(Trip trip) {
        this.trips.put(trip.getId(), trip);
    }
}
