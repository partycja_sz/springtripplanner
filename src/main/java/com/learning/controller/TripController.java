package com.learning.controller;

import com.learning.entity.Trip;
import com.learning.service.TripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/trips")
public class TripController {

    @Autowired
    private TripService tripService;

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Trip> getAllTrips() {
        return tripService.getAllTrips();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Trip getTripById(@PathVariable("id") int id) {
        return tripService.getTripById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteTripById(@PathVariable("id") int id) {
        tripService.deleteTripById(id);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateTrip(@RequestBody Trip trip) {
        tripService.updateTrip(trip);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addTrip(@RequestBody Trip trip) {
        tripService.addTrip(trip);
    }
}
