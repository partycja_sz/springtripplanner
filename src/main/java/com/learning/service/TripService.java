package com.learning.service;

import com.learning.dao.TripDao;
import com.learning.entity.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class TripService {

    @Autowired
    private TripDao tripDao;

    public Collection<Trip> getAllTrips() {
        return this.tripDao.getAllTrips();
    }

    public Trip getTripById(int id) {
        return this.tripDao.getTripById(id);
    }

    public void deleteTripById(int id) {
        this.tripDao.deleteTripById(id);
    }

    public void updateTrip(Trip trip) {
        this.tripDao.updateTrip(trip);
    }

    public void addTrip(Trip trip) {
        this.tripDao.addTrip(trip);
    }
}
